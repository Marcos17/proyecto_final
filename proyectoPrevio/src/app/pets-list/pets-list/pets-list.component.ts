import { Component, OnInit } from '@angular/core';
import { AnimalDatesService } from '../../services/animal-dates/animal-dates.service';


@Component({
  selector: 'app-pets-list',
  templateUrl: './pets-list.component.html',
  styleUrls: ['./pets-list.component.scss']
})
export class PetsListComponent implements OnInit {

  listaMascotas: any;
  clear: any;

  constructor(protected datos:AnimalDatesService) { }


   

  ngOnInit() {
    this.getPets();
  }
    

  getPets(){
    this.datos.getMascotas().subscribe(
      (data) => {
        console.log(data);
        this.listaMascotas = data['mascotas'];
      },
      (error => {
        console.log(error)
      })
    )
  }
}
