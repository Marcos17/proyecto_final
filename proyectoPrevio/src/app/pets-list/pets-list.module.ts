import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PetsListRoutingModule } from './pets-list-routing.module';
import { PetsListComponent } from './pets-list/pets-list.component';




@NgModule({
  declarations: [PetsListComponent],
  imports: [
    CommonModule,
    PetsListRoutingModule,
  
    
  ]
})
export class PetsListModule { }
