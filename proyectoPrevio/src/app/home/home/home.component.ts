import { Component, OnInit } from '@angular/core';
import {RecursosService} from 'src/app/services/recursos/recursos.service'; //llamo el servicio que cree para cargar el json de firebase

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  back: string;   
  play:string;
  arrow: string;
  clear: any; 

  constructor(protected getImage:RecursosService) { }

  ngOnInit() {
    this.getIcon();
  }

  getIcon(){
    this.clear = this.getImage.getRecursos()
      .subscribe((data)=>{
        this.back = data[0].back;
        this.play = data[0].play;
        this.arrow = data[0].arrow;
      }
      )
  }

}
