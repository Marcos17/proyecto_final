import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecursosService {

  constructor(private db:AngularFirestore) { } // se inicializa el service y se guarda con una variable "db".

getRecursos():Observable<any>{   //Observable<any> para que reconozca cualquier tipo de datos 
    return this.db.collection('recursos').valueChanges(); // collection es propio de Firebase,
  }                                                       // luego el nombre d la coleccion que cree en la web y valueChanges para que sea en tiempo real
}
