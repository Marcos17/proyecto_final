import { Injectable } from '@angular/core';
import {Location} from '@angular/common';   

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(public Location : Location) { }

  // devuelve el solo basename de la url
    getLocationPath(){
      let route: string = this.Location.path();
        if (route === '/home' || route === ''){
          return true;
        }else{
          return false;
        }
    }

    getLocation(){
      // devuelve url
      return this.Location;
    }
}
