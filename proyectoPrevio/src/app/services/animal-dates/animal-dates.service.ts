import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AnimalDatesService {

  constructor(private pets:HttpClient) {

   }

   getMascotas(){
    return this.pets.get('https://raw.githubusercontent.com/Marcos170393/mascotas/master/mascotas')
     }
    }

   
