import { TestBed } from '@angular/core/testing';

import { AnimalDatesService } from './animal-dates.service';

describe('AnimalDatesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnimalDatesService = TestBed.get(AnimalDatesService);
    expect(service).toBeTruthy();
  });
});
