import { Component, OnInit } from '@angular/core';
import { RecursosService } from '../../../services/recursos/recursos.service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  face:string;
  twitter:string;
  instagram:string;
  clear:any;

  constructor(protected icons:RecursosService) { }

  ngOnInit() {
     this.getIcon();
  }

  getIcon(){
     this.clear = this.icons.getRecursos()
     .subscribe((data)=>{
       this.face = data[0].face;
       this.twitter = data[0].twitter;
       this.instagram = data[0].inst;

     })
  }

  ngOnDestroy(){
    this.clear.unsuscribe();
  }
}
