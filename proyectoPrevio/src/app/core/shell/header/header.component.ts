import { Component, OnInit } from '@angular/core';
import {RecursosService} from 'src/app/services/recursos/recursos.service'; //llamo el servicio que cree para cargar el json de firebase
import{
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations:[
    trigger(
      'isOpenNav',
      [
        transition(
          ':enter',
          [
            style({transform:'scaleX(0)', opacity:0,}),
            animate('.5s ease-in',
              style({transform:'scaleX(1)', opacity:1,}),
              )
          ]
        ),
        transition(
          ':leave',
          [ 
            style({transform:'scaleX(1)', opacity:1,}),
            animate('.5s ease-out', 
            style({
              transform:'scaleX(0)',
              opacity:0,
            }))
          ]
        )
      ]
    )
  ]
})
export class HeaderComponent implements OnInit {

  isOpen:boolean = false;

  logo: string;
  menuHamb: string;
  close:string;
  clear: any;  // unsuscribe del servicio firebase
  
  constructor(protected getImage:RecursosService) { }  //injecto el servicio

  ngOnInit() {
    this.getIcon();
    
  }

  toggleMenu(){
    this.isOpen = !this.isOpen;
  }

  getIcon(){
    this.clear = this.getImage.getRecursos()
      .subscribe((data)=>{
        this.logo = data[0].Logo;
        this.menuHamb = data[0].menu;
        this.close = data[0].close;
      }
      )
  }
  
  ngOnDestroy(){
    this.clear.unsuscribe();
  }

}
