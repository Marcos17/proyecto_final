import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainWrapperComponent } from './main-wrapper/main-wrapper.component';
import { MainNavigationComponent } from './main-navigation/main-navigation.component';



@NgModule({
  declarations: [MainWrapperComponent, MainNavigationComponent],
  imports: [
    CommonModule
  ],
  exports :[
    MainWrapperComponent,
    MainNavigationComponent,
  ]
})
export class ToolsModule { }
