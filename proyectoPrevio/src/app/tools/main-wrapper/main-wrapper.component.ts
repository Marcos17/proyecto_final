import { Component, OnInit } from '@angular/core';
import {LocationService} from '../../services/location/location.service';


@Component({
  selector: 'app-main-wrapper',
  templateUrl: './main-wrapper.component.html',
  styleUrls: ['./main-wrapper.component.scss']
})
export class MainWrapperComponent implements OnInit {

  constructor(protected pathLocation : LocationService) { };

  pathLoc : boolean; // se almacena el servicio en una variable
  subsUser: any;

  sLocation(){
   this.subsUser = this.pathLoc = this.pathLocation.getLocationPath(); //subsUser,
  };

  ngOnInit() {
    this.sLocation();// cuando se inicia el componente se llama a la funcion
  }

  get printClass(){
    return this.pathLoc ? 'is-home' : 'other-page';
  }

  ngOnDestroy(){
    this.subsUser.unsubscribe(); // esta funcion destruye el servicio, pero antes se desconecta con unsubscribe
  }

 
  

}
