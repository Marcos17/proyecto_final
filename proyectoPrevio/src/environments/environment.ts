// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBnV63s5rZK5FI4yPSdUkFWonP3k6yqmr4",
    authDomain: "petsfinderpro.firebaseapp.com",
    databaseURL: "https://petsfinderpro.firebaseio.com",
    projectId: "petsfinderpro",
    storageBucket: "petsfinderpro.appspot.com",
    messagingSenderId: "877585476242",
    appId: "1:877585476242:web:021bce41ed58acb3fb944b",
    measurementId: "G-XZQ2ECV3GL"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
